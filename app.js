var express = require('express');
var mongoose = require('mongoose');
var http = require('http');
var fql = require('fql');
var graph = require('fbgraph');
var app = express();

var accessToken = '761125843914815|LR_YKrSUiHJ7fg-VU2gkg_NgwO8';

var client_id = '761125843914815';
var client_secret = '92c67bd80fbbcaf606b2bc30cf0021f7';

var server = module.exports = http.createServer(app);
var port = process.env.PORT;
var db = mongoose.createConnection('mongodb://heroku_app21204767:5hMlyYDZs6i0@ds063158.mongolab.com:63158/heroku_app21204767');

var pageIDSchema = mongoose.Schema({fbPageID: String,
                                    limit: Number,
                                    offset: Number,
                                    data : String,
                                    last_update: Date});

var pageSelectedSchema = mongoose.Schema({pageID: String,
                                    pageName: String,
                                    limit: Number,
                                    isSelect: Boolean});

var infoSchema = mongoose.Schema({adID: String,
								  externalAdInfo: String,
								  currentVersion: String});

var serverAccessTokenSchema = mongoose.Schema({key: String, accessToken: String, expires: Number});
app.use(express.bodyParser());

var PageSelectedDatabase = db.model('PageSelectedDatabase', pageSelectedSchema);
var ServerAccessTokenDatabase = db.model('ServerAccessToken', serverAccessTokenSchema);
var InfoDatabase = db.model('Info', infoSchema);

server.listen(port, function() {
  console.log("Listening on " + port);
});

app.get('/*',function(req,res,next){
    res.setHeader("Access-Control-Allow-Origin", "*");
    next(); // http://expressjs.com/guide.html#passing-route control
});

app.post('/*',function(req,res,next){
    res.setHeader("Access-Control-Allow-Origin", "*");
    next(); // http://expressjs.com/guide.html#passing-route control
});

app.get('/', function (req, res) {
	var cus_header = req.header('Content-Identify');
	if (cus_header == 'donsure-111192020') {
 		res.send("Pass");
	} else {
		res.send("Cannot GET /");
	}
});

app.get('/loaderio-962c5e6ee4a6361a8b186563828bd7e3', function (req, res) {
 	res.send('loaderio-962c5e6ee4a6361a8b186563828bd7e3');
});

app.post('/getpage', function (req, res) {
	var cus_header = req.header('Content-Identify');
	if (cus_header == 'donsure-111192020') {
		var pageID = req.body.id;
		var limit = req.body.limit;
		var offset = req.body.offset;

		var PageDatabase = db.model('Page', pageIDSchema);

		PageDatabase.findOne({'fbPageID': pageID,'limit': limit,'offset': offset}, function(e,r) {
			if (r != null) {
				res.send(JSON.parse(r.data));
			} else {
				res.send({});
			}
		});

	} else {
		res.send("Cannot POST /getpage");
	}
});

app.post('/getpagelist', function (req, res) {
	var cus_header = req.header('Content-Identify');
	if (cus_header == 'donsure-111192020') {

		var PageSelectedDatabase = db.model('PageSelectedDatabase', pageSelectedSchema);

	    PageSelectedDatabase.find({}, function (err, docs) {
	    	if (docs != null) {
	    		res.send(docs);
	    	} else {
	    		res.send({});
	    	}
		});
	} else {
		res.send("Cannot POST /getpagelist");
	}
});

app.post('/getinfo', function (req, res) {
	var cus_header = req.header('Content-Identify');
	if (cus_header == 'donsure-111192020') {

	    InfoDatabase.find({}, function (err, docs) {

	    	if (docs != null) {
	    		var newDocs = docs[0];
	    		var externalAdObj = JSON.parse(newDocs.externalAdInfo);
	    		var newDocsPrint = {'adID' : newDocs.adID , 'externalAdInfo' : externalAdObj, 'currentVersion' : newDocs.currentVersion};
	    		res.send(newDocsPrint);
	    	} else {
	    		res.send({});
	    	}
		});
	} else {
		res.send("Cannot POST /getpagelist");
	}
});

app.post('/forceupdatedb', function (req, res) {
	var cus_header = req.header('Content-Identify');
	if (cus_header == 'donsure-111192020') {
		batchDataBaseUpdate();
		res.send("Force Update Database Sent");
	} else {
		res.send("Cannot POST /getpagelist");
	}
});


app.post('/forcesetaccesstoken', function (req, res) {
	var cus_header = req.header('Content-Identify');
	if (cus_header == 'donsure-111192020') {
		accessToken = req.body.accesstoken;
		graph.setAccessToken(accessToken);
		res.send("Force Set Access Token Complete");
	} else {
		res.send("Cannot POST /getpagelist");
	}
});

function updatePageDatabase(pageID, limit, offset) {
	var queryResult1 = 'SELECT object_id,like_info.like_count,pid FROM photo WHERE pid IN (SELECT attachment FROM stream WHERE source_id='+pageID+' AND actor_id='+pageID+' LIMIT '+limit+' OFFSET '+offset+')';
	var queryResult2 = 'SELECT message,attachment,created_time FROM stream WHERE source_id='+pageID+' AND actor_id='+pageID+' LIMIT '+limit+' OFFSET '+offset;

 	fql.query({
	    result1: queryResult1,
	    result2: queryResult2
	}, {
	   	token: accessToken
	}, function(err, data) {
		if (err) {
		    throw err;
		}
		var today = new Date();
		var fbPageObj = {'fbPageID': pageID,'limit': limit,'offset': offset,'data' : JSON.stringify(data),'last_update': today};

		var PageDatabase = db.model('Page', pageIDSchema);

		PageDatabase.findOne({'fbPageID': pageID,'limit': limit,'offset': offset}, function(e,r) {
		  	if (r == null) {
		    	var pageIDObj = new PageDatabase(fbPageObj);
			    pageIDObj.save(function (err) {
			    if (err)
			        console.log('error');
			    });
		    	} else {
		    		
	    			PageDatabase.update({'fbPageID': pageID,'limit': limit,'offset': offset}, {$set: { 'data': JSON.stringify(data),'last_update': today }}, {upsert: true}, function(err) {
			        if (err) {
			        	console.log('It cannot Update!');
			        	}
			        });
		    	}
		    });

	  	console.log("Updated Page ID : "+ pageID + " limit : " + limit + " offset : " + offset);
	});
}


function batchDataBaseUpdate () {
	console.log("Update Database Begin");

	ServerAccessTokenDatabase.find({}, function (err, docs) {
		graph.setAccessToken(docs[0].accessToken);

		graph.extendAccessToken({
		    "client_id": client_id
		  , "client_secret": client_secret
		}, function (err, facebookRes) {
			console.log(facebookRes);
			accessToken = facebookRes.access_token;

			var severTokenObj = new ServerAccessTokenDatabase({"key" : "donsure-key", "accessToken" : accessToken, "expires" : facebookRes.expires});

			ServerAccessTokenDatabase.findOne({"key" : "donsure-key"}, function(e,r) {
				if (r == null) {
					severTokenObj.save(function (err) {
					if (err)
						console.log('error');
					});
				} else {
					ServerAccessTokenDatabase.update({"key" : "donsure-key"}, {$set: { "accessToken" : accessToken, "expires" : facebookRes.expires }}, {upsert: true}, function(err) {
						if (err) {
						    console.log('It cannot Update!');
						}
					});
				}
			});

			PageSelectedDatabase.find({}, 'pageID', function (err, docs) {
				console.log(docs);
			   	for (var i = 0; i < docs.length; i++) {
			   		var findObj = docs[i];
					updatePageDatabase(findObj.pageID, 20, 0);
					updatePageDatabase(findObj.pageID, 20, 20);
					updatePageDatabase(findObj.pageID, 40, 40);
					updatePageDatabase(findObj.pageID, 80, 80);
					updatePageDatabase(findObj.pageID, 160, 160);
					// updatePageDatabase(findObj.pageID, 320, 320);
				};
			});
		});
	});
}

function setUpServer () {
	console.log("Creating New Database");
	var fbPageObj = {'pageID': '160866484017913','pageName': '9GAG in Thai','limit': 14000,'isSelect' : true};
	var pageIDObj = new PageSelectedDatabase(fbPageObj);
	pageIDObj.save(function (err) {
		if (err) console.log('error');
	});

	var severTokenObj = new ServerAccessTokenDatabase({"key" : "donsure-key", "accessToken" : accessToken, "expires" : 1000});

	severTokenObj.save(function (err) {
		if (err) console.log('error');
		console.log("Server Setup Complete");
	});
}

function setUpServer2 () {
	var infoObj = {'adID': '160866484017913','externalAdInfo': JSON.stringify({'ImageLink': 'http://www.google.com', 'link': 'http://www.google.com' }),'currentVersion': '1.1'};
	var infoDBObj = new InfoDatabase(infoObj);
	infoDBObj.save(function (err) {
		if (err) console.log('error');
	});
	console.log("Server Setup 2 Complete");
}

batchDataBaseUpdate();
// setUpServer2();

setInterval(function(){
    batchDataBaseUpdate();
}, 5 * 60 * 1000);